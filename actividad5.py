import os, time

deuda = 100000
contador_productos = 0
saldo_dinamico = 0
while True:
    os.system("cls")
    print("1. Pago tarjeta de credito")
    print("2. Simulacion de compras")
    print("3. Salir")
    try:
        opcion = int(input("Ingrese una opcion\n"))
        if opcion == 1:
            while True:
                os.system("cls")
                print("1. PAGO TARJETITA")
                try:
                    abono = int(input(f"ingrese monto para abonar, su monto facturado es ${deuda}\n"))
                    if abono > 0 and abono < deuda:
                        print(f"monto abonado: ${abono}")
                        saldo = deuda - abono
                        print(f"saldo pendiente: ${saldo}")
                        time.sleep(2)
                        break
                    else:
                        print("monto ingresado excede deuda o es un valor igual o menor a 0")
                        time.sleep(2)  
                        continue              
                except:
                    print("monto ingresado no es un valor numerico")
                    time.sleep(2)
        if opcion == 2:
            try:
                cantidad = int(input("ingrese cantidad de compras a realizar\n"))
                for x in range(cantidad):
                    valor = int(input(f"ingrese monto de la compra {x+1}\n"))
                    if valor > 0 and valor < saldo:
                        contador_productos += 1
                        saldo_dinamico += valor
                    if saldo_dinamico > saldo:
                        print(f"no puedes seguir comprando, el monto excede tu saldo en tarjeta {saldo}")
                        time.sleep(5)
                        break
                print("DETALLE COMPRA")
                print(f"productos comprados: {contador_productos}")
                print(f"monto compra: ${saldo_dinamico}")
                print(f"saldo en tarjeta: ${saldo}")
                time.sleep(5)
            except:
                print("debes ingresar un valor numerico")
                time.sleep(2)
                
    except:
        print("la opcion ingresada no existe, favor ingrese una opcion valida")
        time.sleep(2)
        
####absolutamente en construccion